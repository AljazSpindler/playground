#ifndef VECTOR2_H
#define VECTOR2_H

#include <math.h>

class Vector2
{
public:
	Vector2() { x = 0; y = 0; };
	Vector2(float a, float b) { x = a; y = b; };

	void _addScalar(float scalar) { x += scalar; y += scalar; };
	void _subScalar(float scalar) { x -= scalar; y -= scalar; };
	void _mulScalar(float scalar) { x *= scalar; y *= scalar; };
	void _divScalar(float scalar) { x /= scalar; y /= scalar; };
	void _negate() { x *= -1; y *= -1; };

	void _addVector(Vector2* vec) { x += vec->x; y += vec->y; };
	Vector2* addVector(Vector2* vec) { Vector2* v = new Vector2(x + vec->x, y + vec->y); return v; };
	void _subVector(Vector2* vec) { vec->_negate(); x += vec->x; y += vec->y; };

	float magnitude() { return sqrtf(x * x + y * y); };

	Vector2 norm() { return Vector2(x / magnitude(), y / magnitude()); };

	float dotProduct(Vector2* vec) { return x * vec->x + y * vec->y; };

	float x, y;

private:
};

#endif
