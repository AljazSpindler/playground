#version 430 core

out vec4 FragColor;

in vec3 color;
in vec2 TexCoord;

uniform sampler2D tex1;
uniform vec4 baseColor;

void main()
{
  if(gl_FrontFacing)
  {
    FragColor = texture(tex1, TexCoord);
  }
  else 
  {
    FragColor = vec4(1.0, 0.0, 0.0, 0.5);
  }
}