#pragma once

#include "Point2D.h"
#include <glad\glad.h>

struct Color
{
	float r;
	float g;
	float b;

	Color(float rCol, float gCol, float bCol)
	{
		r = rCol; g = gCol; b = bCol;
	}
};

class Simple2D
{
public:
	Simple2D();
	~Simple2D();

	void DrawRect(Point2D* origin, float width, float height, GLuint* VAO, GLuint* VBO, GLuint* EBO, Color* color, GLint texture, GLint texture2);
	void DrawCircleFilled(Point2D* center, float radius, int segments, GLuint* VBO, Color* color);
	void DrawCircle(Point2D* center, float radius, int segments, GLuint* VBO, Color* color);
	void DrawTriangle(Point2D* a, Point2D* b, Point2D* c, GLuint* VAO, GLuint* VBO, Color* color);
};

