#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include <stdio.h>
#include <cstdlib>
#include "Simple2D.h"
#include "Shader.h"
#include "stb_image.h"
#include "Vector3.h"
#include "Mat4x4.h"
#include "Tranform3D.h"
#include "Cube.h"
#include "FPCamera.h"
#include "Box.h"
#include "Ball.h"
#include "Model.h"
#include "Plane.h"
#include "ScreenQuad.h"
#include "SkyBox.h"

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

Point2D* origin; // Move to objects/actors self position.
float mixTex = 0.2f;

float deltaTime = 0.0f;
float lastFrame = 0.0f;

FPCamera* Cam;
Box* b;
Ball* c;

Simple2D* Simple2d; // Our simple renderer for now :)

int Mode = 0;
bool Wireframe = false;
bool DepthBuffer = false;
bool Outline = false;
int Effect = 0;

void errorCallback(int error, const char* description)
{
	fprintf(stderr, "Error(%d): %s\n", error, description);
}


void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
	else if (key == GLFW_KEY_F1 && action == GLFW_PRESS)
	{
		Mode = 0;
	}
	else if (key == GLFW_KEY_F2 && action == GLFW_PRESS)
	{
		Mode = 1;
	}
	else if (key == GLFW_KEY_F3 && action == GLFW_PRESS)
	{
		Mode = 2;
	}
	else if (key == GLFW_KEY_F4 && action == GLFW_PRESS)
	{
		Mode = 3;
	}
	else if (key == GLFW_KEY_F5 && action == GLFW_PRESS)
	{
		Wireframe = !Wireframe;
	}
	else if (key == GLFW_KEY_F6 && action == GLFW_PRESS)
	{
		DepthBuffer = !DepthBuffer;
	}
	else if (key == GLFW_KEY_F7 && action == GLFW_PRESS)
	{
		Outline = !Outline;
	}
	else if (key == GLFW_KEY_F8 && action == GLFW_PRESS)
	{
		Mode = 4;
	}
	else if (key == GLFW_KEY_F9 && action == GLFW_PRESS)
	{
		Mode = 5;
	}
	else if (key == GLFW_KEY_0 && action == GLFW_PRESS)
	{
		Effect = 0;
	}
	else if (key == GLFW_KEY_1 && action == GLFW_PRESS)
	{
		Effect = 1;
	}
	else if (key == GLFW_KEY_2 && action == GLFW_PRESS)
	{
		Effect = 2;
	}
	else if (key == GLFW_KEY_3 && action == GLFW_PRESS)
	{
		Effect = 3;
	}
	else if (key == GLFW_KEY_4 && action == GLFW_PRESS)
	{
		Effect = 4;
	}
	else if (key == GLFW_KEY_5 && action == GLFW_PRESS)
	{
		Effect = 5;
	}
	else if (key == GLFW_KEY_6 && action == GLFW_PRESS) // Explode.
	{
		Effect = 6;
	}
	else
	{
		if (Mode == 0 || Mode == 2 || Mode == 3 || Mode == 4 || Mode == 5)
		{
			Cam->Move(key, action, deltaTime);
		}
		else
		{
			b->Move(key, action, deltaTime);
			c->Move(key, action, deltaTime);
		}
	}

}

void mouseCallback(GLFWwindow* window, double xPos, double yPos)
{
	if (Mode == 0 || Mode == 2 || Mode == 3 || Mode == 4 || Mode == 5)
	{
		Cam->Look(xPos, yPos);
	}
	else
	{
		b->Look(xPos, yPos);
		c->Look(xPos, yPos);
	}
}

void scrollCallback(GLFWwindow* window, double xOffset, double yOffset)
{
	if (Mode == 0 || Mode == 2 ||Mode == 3 || Mode == 4 || Mode == 5)
	{
		Cam->Zoom(xOffset, yOffset);
	}
	else
	{
		b->Zoom(xOffset, yOffset);
		c->Zoom(xOffset, yOffset);
	}
}

void checkCollision(Box* b, Ball* c)
{
	float cXPos = c->position->x;
	float cYPos = c->position->y;

	float bXPos = b->position->x;
	float bYPos = b->position->y;

	float xDiff = cXPos - bXPos;
	float yDiff = cYPos - bYPos;

	if (abs(xDiff) + abs(yDiff) <= (2 * c->radius))
	{
		if (abs(xDiff) <= c->radius)
		{
			if (xDiff < 0)
			{
				c->position->x -= c->bounce;
				b->position->x += b->bounce;
			}
			else
			{
				c->position->x += c->bounce;
				b->position->x -= b->bounce;
			}
		}

		if (abs(yDiff) <= c->radius)
		{
			if (yDiff < 0)
			{
				c->position->y -= c->bounce;
				b->position->y += b->bounce;
			}
			else
			{
				c->position->y += c->bounce;
				b->position->y -= b->bounce;
			}
		}
	}
}

unsigned int TextureFromFile(const char* path)
{
	int texWidth, texHeight, nrChannels;
	unsigned char *data = stbi_load(path, &texWidth, &texHeight, &nrChannels, 0);

	unsigned int texture;
	glGenTextures(1, &texture);


	if (data)
	{
		GLenum format;
		if (nrChannels == 1)
		{
			format = GL_RED;
		}
		else if (nrChannels == 3)
		{
			format = GL_RGB;
		}
		else if (nrChannels == 4)
		{
			format = GL_RGBA;
		}

		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, format, texWidth, texHeight, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else
	{
		std::cout << "Failed to load texture at: " << path << std::endl;
	}

	stbi_image_free(data);

	return texture;
}

unsigned int CubemapFromFile(std::vector<const char*> paths)
{
	unsigned int textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureId);

	int texWidth, texHeight, nrChannels;
	for (unsigned int i = 0; i < paths.size(); i++)
	{
		unsigned char *data = stbi_load(paths[i], &texWidth, &texHeight, &nrChannels, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, texWidth, texHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		}
		else
		{
			std::cout << "Failed to load cubemap texture at: " << paths[i] << std::endl;
		}

		stbi_image_free(data);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return textureId;
}

void CheckOpenGlError()
{
	GLenum err;
	while ((err = glGetError()) != GL_NO_ERROR) {
		std::cerr << "OpenGL error: " << err << std::endl;
	}
}

int main()
{
	glfwSetErrorCallback(errorCallback);

	if (!glfwInit())
	{
		return -1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);
	GLFWwindow* window = glfwCreateWindow(800, 600, "ItNTH", NULL, NULL);
	if (!window)
	{
		return -1;
	}

	glfwSetKeyCallback(window, keyCallback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(window, mouseCallback);
	glfwSetScrollCallback(window, scrollCallback);
	glfwMakeContextCurrent(window);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		return -1;
	}
	glfwSwapInterval(1);

	//glEnable(GL_DEBUG_OUTPUT);

	Cam = new FPCamera(Vector3(0.0f, 0.0f, 3.0f), Vector3(0.0f, 0.0f, -1.0f), Vector3(0.0f, 1.0f, 0.0f), 8.5f, 0.05f);

	Simple2d = new Simple2D();
	origin = new Point2D(0.0f, 0.0f, 0.0f);

	Shader theShader("C:/Playground/ItNTH/ItNTH/VertShader.glsl", "C:/Playground/ItNTH/ItNTH/FragShader.glsl");
	Shader lightingShader("C:/Playground/ItNTH/ItNTH/VertShaderLight.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderLight.glsl");
	Shader lampShader("C:/Playground/ItNTH/ItNTH/VertShaderLight.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderLamp.glsl");
	Shader nanoShader("C:/Playground/ItNTH/ItNTH/VertShaderNano.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderNano.glsl");

	Shader depthShaderLight("C:/Playground/ItNTH/ItNTH/VertShaderLight.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderDepth.glsl");
	Shader depthShaderNano("C:/Playground/ItNTH/ItNTH/VertShaderNano.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderDepth.glsl");

	Shader outlineShader("C:/Playground/ItNTH/ItNTH/VertShaderLight.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderSingleCol.glsl");

	Shader screenShader("C:/Playground/ItNTH/ItNTH/VertShaderScreen.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderScreen.glsl");
	Shader screenShaderInversion("C:/Playground/ItNTH/ItNTH/VertShaderScreen.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderScreenInv.glsl");
	Shader screenShaderGray("C:/Playground/ItNTH/ItNTH/VertShaderScreen.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderScreenGray.glsl");
	Shader screenShaderSharpen("C:/Playground/ItNTH/ItNTH/VertShaderScreen.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderScreenSharpen.glsl");
	Shader screenShaderBlur("C:/Playground/ItNTH/ItNTH/VertShaderScreen.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderScreenBlur.glsl");
	Shader screenShaderEdge("C:/Playground/ItNTH/ItNTH/VertShaderScreen.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderScreenEdge.glsl");

	Shader skyboxShader("C:/Playground/ItNTH/ItNTH/VertShaderSkyBox.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderSkyBox.glsl");

	Shader pointSizeShader("C:/Playground/ItNTH/ItNTH/VertShaderPointSize.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderBlock.glsl", "C:/Playground/ItNTH/ItNTH/GeomShaderPointSize.glsl");
	Shader nanoShaderExplode("C:/Playground/ItNTH/ItNTH/VertShaderNano.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderNano.glsl", "C:/Playground/ItNTH/ItNTH/GeomShaderExplode.glsl");

	Shader nanoShaderNormal("C:/Playground/ItNTH/ItNTH/VertShaderNanoNormal.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderNanoNormal.glsl", "C:/Playground/ItNTH/ItNTH/GeomShaderExplodeNormal.glsl");

	Shader uniformBuff1Shader("C:/Playground/ItNTH/ItNTH/VertShaderUniform.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderUniform1.glsl");
	Shader uniformBuff2Shader("C:/Playground/ItNTH/ItNTH/VertShaderUniform.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderUniform2.glsl");
	Shader uniformBuff3Shader("C:/Playground/ItNTH/ItNTH/VertShaderUniform.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderUniform3.glsl");
	Shader uniformBuff4Shader("C:/Playground/ItNTH/ItNTH/VertShaderUniform.glsl", "C:/Playground/ItNTH/ItNTH/FragShaderUniform4.glsl");

	unsigned int texture1 = TextureFromFile("C:/Playground/ItNTH/ItNTH/resources/container.jpg");	
	unsigned int texture2 = TextureFromFile("C:/Playground/ItNTH/ItNTH/resources/awesomeface.png");
	unsigned int texture3 = TextureFromFile("C:/Playground/ItNTH/ItNTH/resources/container2.png");
	unsigned int texture4 = TextureFromFile("C:/Playground/ItNTH/ItNTH/resources/container2_specular.png");
	unsigned int textureGrass = TextureFromFile("C:/Playground/ItNTH/ItNTH/resources/grass.png");
	unsigned int textureWindow = TextureFromFile("C:/Playground/ItNTH/ItNTH/resources/blending_transparent_window.png");

	std::vector<const char*> faces
	{
		"C:/Playground/ItNTH/ItNTH/resources/Skybox/right.jpg",
		"C:/Playground/ItNTH/ItNTH/resources/Skybox/left.jpg",
		"C:/Playground/ItNTH/ItNTH/resources/Skybox/top.jpg",
		"C:/Playground/ItNTH/ItNTH/resources/Skybox/bottom.jpg",
		"C:/Playground/ItNTH/ItNTH/resources/Skybox/back.jpg",
		"C:/Playground/ItNTH/ItNTH/resources/Skybox/front.jpg"
	};

	unsigned int cubemapTex = CubemapFromFile(faces);
	SkyBox* sky = new SkyBox(cubemapTex);

	CheckOpenGlError();


	Point2D* trigA = new Point2D(-0.2f, 0.0f, 0.0f);
	Point2D* trigC = new Point2D(-0.1f, 0.5f, 0.0f);
	Point2D* circleCenterFilled = new Point2D(0.4f, 0.53f, 0.0f);
	Point2D* circleCenter = new Point2D(0.75f, 0.8f, 0.0f);
	Color* col = new Color(1.0f, 0.5f, 0.3f);

	int width, height;

	glfwGetFramebufferSize(window, &width, &height);

	c = new Ball(new Point2D(60.0f, 60.0f, 0.0f), 30.0f, col, texture1, 300.0f);
	b = new Box(origin, 50.0f, 50.0f, col, texture1, 300.0f);

	Cube* d = new Cube(col, texture3, texture4, new Vector3(0.0f, 0.0f, 0.0f));


	Cube* light = new Cube(col, -1, -1, new Vector3(1.2f, 1.0f, -2.0f));


	Vector3 positions[10] =
	{
		Vector3(0.0f,  0.0f,  0.0f),
		Vector3(2.0f,  5.0f, -15.0f),
		Vector3(-1.5f, -2.2f, -2.5f),
		Vector3(-3.8f, -2.0f, -12.3f),
		Vector3(2.4f, -0.4f, -3.5f),
		Vector3(-1.7f,  3.0f, -7.5f),
		Vector3(1.3f, -2.0f, -2.5f),
		Vector3(1.5f,  2.0f, -2.5f),
		Vector3(1.5f,  0.2f, -1.5f),
		Vector3(-1.3f,  1.0f, -1.5f)
	};

	Vector3 pointLightPositions[4] =
	{
		Vector3(0.7f, 0.2f, 2.0f),
		Vector3(2.3f, -3.3f, -4.0f),
		Vector3(-4.0f, 2.0f, -12.0f),
		Vector3(0.0f, 0.0f, -3.0f)
	};

	Vector3 windowPositions[4] =
	{
		Vector3(0.7f, 0.2f, -3.0f),
		Vector3(1.3f, -1.3f, -2.0f),
		Vector3(-1.0f, 2.0f, -1.0f),
		Vector3(0.5f, 0.0f, 1.0f)
	};


	Model* m = new Model("C:/Playground/ItNTH/ItNTH/resources/Nanosuit/nanosuit.obj");

	Model* m2 = new Model("C:/Playground/ItNTH/ItNTH/resources/Nanosuit/nanosuit.obj");

	Plane* pl = new Plane(new Vector3(0.0f, 0.0f, 0.0f), new Color(1.0f, 0.0f, 0.0f), textureGrass, -1, new Vector3(1.2f, 1.0f, -2.0f), 100.0f, 100.0f);
	
	Plane* w = new Plane(new Vector3(0.0f, 0.0f, 0.0f), new Color(1.0f, 0.0f, 0.0f), textureWindow, -1, new Vector3(1.2f, 1.0f, -2.0f), 100.0f, 100.0f);

	CheckOpenGlError();

	unsigned int framebuffer;
	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	unsigned int texColorBuffer;
	glGenTextures(1, &texColorBuffer);
	glBindTexture(GL_TEXTURE_2D, texColorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 800, 600, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);


	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texColorBuffer, 0);


	unsigned int rbo;
	glGenRenderbuffers(1, &rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 800, 600);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "FRAMEBUFFER ERROR!" << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	unsigned int uniformBlockIndex1 = glGetUniformBlockIndex(uniformBuff1Shader.Id, "Matrices");
	unsigned int uniformBlockIndex2 = glGetUniformBlockIndex(uniformBuff2Shader.Id, "Matrices");
	unsigned int uniformBlockIndex3 = glGetUniformBlockIndex(uniformBuff3Shader.Id, "Matrices");
	unsigned int uniformBlockIndex4 = glGetUniformBlockIndex(uniformBuff4Shader.Id, "Matrices");

	glUniformBlockBinding(uniformBuff1Shader.Id, uniformBlockIndex1, 0);
	glUniformBlockBinding(uniformBuff2Shader.Id, uniformBlockIndex2, 0);
	glUniformBlockBinding(uniformBuff3Shader.Id, uniformBlockIndex3, 0);
	glUniformBlockBinding(uniformBuff4Shader.Id, uniformBlockIndex4, 0);

	unsigned int uboMatrices;
	glGenBuffers(1, &uboMatrices);

	while (!glfwWindowShouldClose(window))
	{
		float ratio;

		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		glViewport(0, 0, width, height);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_MULTISAMPLE);

		glDepthFunc(GL_LESS);

		if (Wireframe)
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Wireframe mode.
		}
		else
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}

		if (Outline)
		{
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
			glEnable(GL_STENCIL_TEST);
			glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
			glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
			glStencilMask(0x00);
		}
		else
		{
			glDisable(GL_STENCIL_TEST);
		}

		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		float time = glfwGetTime();

		deltaTime = time - lastFrame;
		lastFrame = time;
		CheckOpenGlError();

		if (Mode == 1)
		{
			theShader.use();
			/*for (unsigned int i = 0; i < 5; i++)
			{
				std::stringstream ss;
				std::string index;
				ss << i;
				index = ss.str();
				theShader.setVec3(("offsets[" + index + "]").c_str(), instanceLocs[i].x, instanceLocs[i].y, instanceLocs[i].z);
			}*/
			//checkCollision(b, c);

			Mat4x4* model = Transform3D::GetTranslationMat(b->position->x, b->position->y, b->position->z);
			theShader.setMatrix4x4("model", model->matrix, GL_TRUE);

			Mat4x4* projection = Transform3D::GetOrthoProjMat(0.0f, 800.0f, 0.0f, 600.0f, -1.0f, 1.0f);
			Mat4x4* view = new Mat4x4();
			view->_identity();

			theShader.setMatrix4x4("projection", projection->matrix, GL_TRUE);
			theShader.setMatrix4x4("view", view->matrix, GL_TRUE);
			theShader.setInt("tex1", 0);
			theShader.setFloat4("baseColor", 1.0f, 1.0f, 1.0f, 1.0f);

			b->Draw();

			//model = Transform3D::GetTranslationMat(c->position->x, c->position->y, c->position->z);
			//theShader.setMatrix4x4("model", model->matrix, GL_TRUE);
			//c->Draw();

			//std::cout << "Ball Pos: " << b->position->x << ":" << b->position->y << std::endl;
			//std::cout << "Box Pos: " << c->position->x << ":" << c->position->y << std::endl;
		}
		else if (Mode == 0)
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			Mat4x4* projection = Transform3D::GetPerspectiveProjMat(Cam->GetFov(), width / (float)height, 0.1f, 100.0f);
			Mat4x4* view = Cam->GetLookAtMat();

			if (DepthBuffer)
			{
				depthShaderLight.use();
			}
			else
			{
				lightingShader.use();
			}

			if (Outline)
			{
				glStencilFunc(GL_ALWAYS, 1, 0xFF);
				glStencilMask(0xFF);
			}

			CheckOpenGlError();

			Mat4x4* model = Transform3D::GetTranslationMat(d->position->x, d->position->y, d->position->z);

			lightingShader.setMatrix4x4("model", model->matrix, GL_TRUE);
			lightingShader.setMatrix4x4("view", view->matrix, GL_TRUE);
			lightingShader.setMatrix4x4("projection", projection->matrix, GL_TRUE);

			lightingShader.setVec3("viewPos", Cam->GetPos()->x, Cam->GetPos()->y, Cam->GetPos()->z);
			
			lightingShader.setInt("material.diffuse", 0);
			lightingShader.setInt("material.specular", 1);
			lightingShader.setFloat("material.shininess", 32.0f);

			lightingShader.setVec3("dirLight.ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
			lightingShader.setVec3("dirLight.specular", 0.5f, 0.5f, 0.5f);
			lightingShader.setVec3("dirLight.direction", -0.2f, -1.0f, -0.3f);

			
			float angleRadians = 7.5f * (PI / 180.0f);
			float outerAngleRadians = 12.5f * (PI / 180.0f);
			lightingShader.setVec3("spotLight.ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("spotLight.diffuse", 0.5f, 0.5f, 0.5f);
			lightingShader.setVec3("spotLight.specular", 0.5f, 0.5f, 0.5f);
			lightingShader.setVec3("spotLight.position", Cam->GetPos()->x, Cam->GetPos()->y, Cam->GetPos()->z);
			lightingShader.setVec3("spotLight.direction", Cam->GetFront()->x, Cam->GetFront()->y, Cam->GetFront()->z);
			lightingShader.setFloat("spotLight.cutOff", cosf(angleRadians));
			lightingShader.setFloat("spotLight.outerCutOff", cosf(outerAngleRadians));
			lightingShader.setFloat("spotLight.constant", 1.0f);
			lightingShader.setFloat("spotLight.linear", 0.09f);
			lightingShader.setFloat("spotLight.quadratic", 0.032f);
			

			lightingShader.setVec3("pointLights[0].position", positions[0].x, positions[0].y, positions[0].z);
			lightingShader.setVec3("pointLights[0].ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("pointLights[0].diffuse", 0.8f, 0.8f, 0.8f);
			lightingShader.setVec3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
			lightingShader.setFloat("pointLights[0].constant", 1.0f);
			lightingShader.setFloat("pointLights[0].linear", 0.09f);
			lightingShader.setFloat("pointLights[0].quadratic", 0.032f);

			lightingShader.setVec3("pointLights[1].position", positions[1].x, positions[1].y, positions[1].z);
			lightingShader.setVec3("pointLights[1].ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("pointLights[1].diffuse", 0.8f, 0.8f, 0.8f);
			lightingShader.setVec3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
			lightingShader.setFloat("pointLights[1].constant", 1.0f);
			lightingShader.setFloat("pointLights[1].linear", 0.09f);
			lightingShader.setFloat("pointLights[1].quadratic", 0.032f);

			lightingShader.setVec3("pointLights[2].position", positions[2].x, positions[2].y, positions[2].z);
			lightingShader.setVec3("pointLights[2].ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("pointLights[2].diffuse", 0.8f, 0.8f, 0.8f);
			lightingShader.setVec3("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
			lightingShader.setFloat("pointLights[2].constant", 1.0f);
			lightingShader.setFloat("pointLights[2].linear", 0.09f);
			lightingShader.setFloat("pointLights[2].quadratic", 0.032f);

			lightingShader.setVec3("pointLights[3].position", positions[3].x, positions[3].y, positions[3].z);
			lightingShader.setVec3("pointLights[3].ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("pointLights[3].diffuse", 0.8f, 0.8f, 0.8f);
			lightingShader.setVec3("pointLights[3].specular", 1.0f, 1.0f, 1.0f);
			lightingShader.setFloat("pointLights[3].constant", 1.0f);
			lightingShader.setFloat("pointLights[3].linear", 0.09f);
			lightingShader.setFloat("pointLights[3].quadratic", 0.032f);
			glEnable(GL_CULL_FACE);

			for (unsigned int i = 0; i < 10; i++)
			{
				model = Transform3D::GetTranslationMat(positions[i].x, positions[i].y, positions[i].z);

				lightingShader.setMatrix4x4("model", model->matrix, GL_TRUE);
				d->Draw(false);
			}

			CheckOpenGlError();

			// Outlining
			if (Outline)
			{
				glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
				glStencilMask(0x00);
				glDisable(GL_DEPTH_TEST);

				outlineShader.use();

				outlineShader.setMatrix4x4("view", view->matrix, GL_TRUE);
				outlineShader.setMatrix4x4("projection", projection->matrix, GL_TRUE);

				for (unsigned int i = 0; i < 10; i++)
				{
					model = Transform3D::GetTranslationMat(positions[i].x, positions[i].y, positions[i].z);
					model = model->mulMatrix(Transform3D::GetNonUniformScaleMat(1.02f, 1.02f, 1.02f));

					outlineShader.setMatrix4x4("model", model->matrix, GL_TRUE);
					d->Draw(false);
				}

				glStencilMask(0xFF);
				glEnable(GL_DEPTH_TEST);
			}

			// Plane and lights.
			// Use grass shader.
			glDisable(GL_CULL_FACE);
			theShader.use();

			theShader.setMatrix4x4("projection", projection->matrix, GL_TRUE);
			theShader.setMatrix4x4("view", view->matrix, GL_TRUE);
			theShader.setInt("tex1", 0);
			theShader.setFloat4("baseColor", 1.0f, 1.0f, 1.0f, 1.0f);

			model = Transform3D::GetTranslationMat(pl->center->x, pl->center->y + 2.0f, pl->center->z);
			model = model->mulMatrix(Transform3D::GetRotationYMat(90.0f))->mulMatrix(Transform3D::GetRotationXMat(180.0f));

			theShader.setMatrix4x4("model", model->matrix, GL_TRUE);


			pl->Draw();

			for (unsigned int i = 0; i < 4; i++)
			{
				model = Transform3D::GetTranslationMat(windowPositions[i].x, windowPositions[i].y, windowPositions[i].z)->mulMatrix(Transform3D::GetRotationYMat(90.0f));
				theShader.setMatrix4x4("model", model->matrix, GL_TRUE);
				w->Draw();
			}

			/*
			lampShader.use();

			lampShader.setMatrix4x4("model", model->matrix, GL_TRUE);
			lampShader.setMatrix4x4("view", view->matrix, GL_TRUE);
			lampShader.setMatrix4x4("projection", projection->matrix, GL_TRUE);
			
			for (unsigned int i = 0; i < 4; i++)
			{
				model = Transform3D::GetTranslationMat(pointLightPositions[i].x, pointLightPositions[i].y, pointLightPositions[i].z);
				model = model->mulMatrix(Transform3D::GetNonUniformScaleMat(0.5f, 0.5f, 0.5f));
				lampShader.setMatrix4x4("model", model->matrix, GL_TRUE);
		        light->Draw();
			}*/

			glEnable(GL_DEPTH_TEST);

			skyboxShader.use();
			view->matrix[3] = 0; view->matrix[7] = 0; view->matrix[11] = 0; view->matrix[15] = 0;

			skyboxShader.setMatrix4x4("view", view->matrix, GL_TRUE);
			skyboxShader.setMatrix4x4("projection", projection->matrix, GL_TRUE);
			skyboxShader.setInt("skybox", 0);

			sky->Draw();

		}
		else if (Mode == 2)
		{
			if (DepthBuffer)
			{
				depthShaderNano.use();
			}
			else
			{
				nanoShader.use();
			}

			//if (Effect == 6)
			//{
			///	nanoShaderExplode.use();
			//}

			Mat4x4* model = Transform3D::GetTranslationMat(0.0f, -1.75f, 0.0f);
			model = model->mulMatrix(Transform3D::GetNonUniformScaleMat(0.2f, 0.2f, 0.2f));		

			nanoShader.setMatrix4x4("model", model->matrix, GL_TRUE);

			Mat4x4* projection = Transform3D::GetPerspectiveProjMat(Cam->GetFov(), width / (float)height, 0.1f, 100.0f);
			Mat4x4* view = Cam->GetLookAtMat();

			nanoShader.setMatrix4x4("projection", projection->matrix, GL_TRUE);
			nanoShader.setMatrix4x4("view", view->matrix, GL_TRUE);
			//nanoShader.setFloat("time", glfwGetTime());

			CheckOpenGlError();

			m->Draw(nanoShader);

			model = Transform3D::GetTranslationMat(0.0f, -5.75f, 0.0f);
			model = model->mulMatrix(Transform3D::GetNonUniformScaleMat(0.3f, 0.3f, 0.3f));
			nanoShader.setMatrix4x4("model", model->matrix, GL_TRUE);

			m2->Draw(nanoShader);

			// Normals
			nanoShaderNormal.use();

			nanoShaderNormal.setMatrix4x4("model", model->matrix, GL_TRUE);
			nanoShaderNormal.setMatrix4x4("projection", projection->matrix, GL_TRUE);
			nanoShaderNormal.setMatrix4x4("view", view->matrix, GL_TRUE);

			m->Draw(nanoShaderNormal);

			glEnable(GL_DEPTH_TEST);

			skyboxShader.use();
			view->matrix[3] = 0; view->matrix[7] = 0; view->matrix[11] = 0; view->matrix[15] = 0;

			skyboxShader.setMatrix4x4("view", view->matrix, GL_TRUE);
			skyboxShader.setMatrix4x4("projection", projection->matrix, GL_TRUE);
			skyboxShader.setInt("skybox", 0);

			sky->Draw();
		}
		else if (Mode == 3) // Framebuffering.
		{
			glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
			glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glEnable(GL_DEPTH_TEST);

	        lightingShader.use();

			CheckOpenGlError();

			Mat4x4* projection = Transform3D::GetPerspectiveProjMat(Cam->GetFov(), width / (float)height, 0.1f, 100.0f);
			Mat4x4* view = Cam->GetLookAtMat();
			Mat4x4* model = Transform3D::GetTranslationMat(d->position->x, d->position->y, d->position->z);

			lightingShader.setMatrix4x4("model", model->matrix, GL_TRUE);
			lightingShader.setMatrix4x4("view", view->matrix, GL_TRUE);
			lightingShader.setMatrix4x4("projection", projection->matrix, GL_TRUE);

			lightingShader.setVec3("viewPos", Cam->GetPos()->x, Cam->GetPos()->y, Cam->GetPos()->z);

			lightingShader.setInt("material.diffuse", 0);
			lightingShader.setInt("material.specular", 1);
			lightingShader.setFloat("material.shininess", 32.0f);

			lightingShader.setVec3("dirLight.ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
			lightingShader.setVec3("dirLight.specular", 0.5f, 0.5f, 0.5f);
			lightingShader.setVec3("dirLight.direction", -0.2f, -1.0f, -0.3f);


			float angleRadians = 7.5f * (PI / 180.0f);
			float outerAngleRadians = 12.5f * (PI / 180.0f);
			lightingShader.setVec3("spotLight.ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("spotLight.diffuse", 0.5f, 0.5f, 0.5f);
			lightingShader.setVec3("spotLight.specular", 0.5f, 0.5f, 0.5f);
			lightingShader.setVec3("spotLight.position", Cam->GetPos()->x, Cam->GetPos()->y, Cam->GetPos()->z);
			lightingShader.setVec3("spotLight.direction", Cam->GetFront()->x, Cam->GetFront()->y, Cam->GetFront()->z);
			lightingShader.setFloat("spotLight.cutOff", cosf(angleRadians));
			lightingShader.setFloat("spotLight.outerCutOff", cosf(outerAngleRadians));
			lightingShader.setFloat("spotLight.constant", 1.0f);
			lightingShader.setFloat("spotLight.linear", 0.09f);
			lightingShader.setFloat("spotLight.quadratic", 0.032f);


			lightingShader.setVec3("pointLights[0].position", positions[0].x, positions[0].y, positions[0].z);
			lightingShader.setVec3("pointLights[0].ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("pointLights[0].diffuse", 0.8f, 0.8f, 0.8f);
			lightingShader.setVec3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
			lightingShader.setFloat("pointLights[0].constant", 1.0f);
			lightingShader.setFloat("pointLights[0].linear", 0.09f);
			lightingShader.setFloat("pointLights[0].quadratic", 0.032f);

			lightingShader.setVec3("pointLights[1].position", positions[1].x, positions[1].y, positions[1].z);
			lightingShader.setVec3("pointLights[1].ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("pointLights[1].diffuse", 0.8f, 0.8f, 0.8f);
			lightingShader.setVec3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
			lightingShader.setFloat("pointLights[1].constant", 1.0f);
			lightingShader.setFloat("pointLights[1].linear", 0.09f);
			lightingShader.setFloat("pointLights[1].quadratic", 0.032f);

			lightingShader.setVec3("pointLights[2].position", positions[2].x, positions[2].y, positions[2].z);
			lightingShader.setVec3("pointLights[2].ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("pointLights[2].diffuse", 0.8f, 0.8f, 0.8f);
			lightingShader.setVec3("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
			lightingShader.setFloat("pointLights[2].constant", 1.0f);
			lightingShader.setFloat("pointLights[2].linear", 0.09f);
			lightingShader.setFloat("pointLights[2].quadratic", 0.032f);

			lightingShader.setVec3("pointLights[3].position", positions[3].x, positions[3].y, positions[3].z);
			lightingShader.setVec3("pointLights[3].ambient", 0.1f, 0.1f, 0.1f);
			lightingShader.setVec3("pointLights[3].diffuse", 0.8f, 0.8f, 0.8f);
			lightingShader.setVec3("pointLights[3].specular", 1.0f, 1.0f, 1.0f);
			lightingShader.setFloat("pointLights[3].constant", 1.0f);
			lightingShader.setFloat("pointLights[3].linear", 0.09f);
			lightingShader.setFloat("pointLights[3].quadratic", 0.032f);
			glEnable(GL_CULL_FACE);

			for (unsigned int i = 0; i < 10; i++)
			{
				model = Transform3D::GetTranslationMat(positions[i].x, positions[i].y, positions[i].z);

				lightingShader.setMatrix4x4("model", model->matrix, GL_TRUE);
				d->Draw(false);
			}

			glDisable(GL_CULL_FACE);

			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			if (Effect == 0)
			{
				screenShader.use();
			}
			else if (Effect == 1)
			{
				screenShaderInversion.use();
			}
			else if (Effect == 2)
			{
				screenShaderGray.use();
			}
			else if (Effect == 3)
			{
				screenShaderSharpen.use();
			}
			else if (Effect == 4)
			{
				screenShaderBlur.use();
			}
			else if (Effect == 5)
			{
				screenShaderEdge.use();
			}
			else
			{
				screenShader.use();
			}
			ScreenQuad* sQuad = new ScreenQuad(texColorBuffer);
			sQuad->Draw();
		}
		else if (Mode == 4)
		{
			glEnable(GL_PROGRAM_POINT_SIZE);

			Mat4x4* projection = Transform3D::GetPerspectiveProjMat(Cam->GetFov(), width / (float)height, 0.1f, 100.0f);
			Mat4x4* view = Cam->GetLookAtMat();
			Mat4x4* model = Transform3D::GetTranslationMat(d->position->x, d->position->y, d->position->z);

			pointSizeShader.use();

			pointSizeShader.setMatrix4x4("model", model->matrix, GL_TRUE);
			pointSizeShader.setMatrix4x4("view", view->matrix, GL_TRUE);
			pointSizeShader.setMatrix4x4("projection", projection->matrix, GL_TRUE);

			pointSizeShader.setInt("tex1", 0);
			pointSizeShader.setFloat4("baseColor", 1.0f, 1.0f, 1.0f, 1.0f);

			d->Draw(true);
		}
		else if (Mode == 5)
		{
			Mat4x4* projection = Transform3D::GetPerspectiveProjMat(Cam->GetFov(), width / (float)height, 0.1f, 100.0f);
			Mat4x4* view = Cam->GetLookAtMat();
		
			glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
			glBufferData(GL_UNIFORM_BUFFER, 32 * sizeof(float), NULL, GL_STATIC_DRAW);
			glBindBuffer(GL_UNIFORM_BUFFER, 0);

			glBindBufferRange(GL_UNIFORM_BUFFER, 0, uboMatrices, 0, 32 * sizeof(float));

			glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
			glBufferSubData(GL_UNIFORM_BUFFER, 0, 16 * sizeof(float), view->matrix);
			glBindBuffer(GL_UNIFORM_BUFFER, 0);

			glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
			glBufferSubData(GL_UNIFORM_BUFFER, 16 * sizeof(float), 16 * sizeof(float), projection->matrix);
			glBindBuffer(GL_UNIFORM_BUFFER, 0);
			
			Mat4x4* model = Transform3D::GetTranslationMat(d->position->x, d->position->y, d->position->z);

			uniformBuff1Shader.use();

			uniformBuff1Shader.setMatrix4x4("model", model->matrix, GL_TRUE);

			d->Draw(false);

			/*Mat4x4* model2 = model->mulMatrix(Transform3D::GetTranslationMat(1.0f, 0.0f, 0.0f));

			uniformBuff2Shader.use();

			uniformBuff2Shader.setMatrix4x4("model", model2->matrix, GL_TRUE);
			uniformBuff2Shader.setInt("tex1", 0);
			uniformBuff2Shader.setFloat4("baseColor", 1.0f, 1.0f, 1.0f, 1.0f);

			d->Draw(false);

			model2 = model->mulMatrix(Transform3D::GetTranslationMat(-1.0f, 0.0f, 0.0f));

			uniformBuff3Shader.use();

			uniformBuff3Shader.setMatrix4x4("model", model2->matrix, GL_TRUE);
			uniformBuff3Shader.setInt("tex1", 0);
			uniformBuff3Shader.setFloat4("baseColor", 1.0f, 1.0f, 1.0f, 1.0f);

			d->Draw(false);

			model2 = model->mulMatrix(Transform3D::GetTranslationMat(-2.0f, 0.0f, 0.0f));

			uniformBuff4Shader.use();

			uniformBuff4Shader.setMatrix4x4("model", model2->matrix, GL_TRUE);
			uniformBuff4Shader.setInt("tex1", 0);
			uniformBuff4Shader.setFloat4("baseColor", 1.0f, 1.0f, 1.0f, 1.0f);
			
			d->Draw(false);*/
		}

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);

	glfwTerminate();

	return 0;
}
