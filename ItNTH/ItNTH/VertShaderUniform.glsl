#version 430 core

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 inColor;
layout (location = 2) in vec2 inTexCoord;

layout (std140) uniform Matrices
{
  mat4 view;
  mat4 projection;
};

uniform mat4 model;

void main()
{
  gl_Position = projection * view * model * vec4(Position, 1.0);
}