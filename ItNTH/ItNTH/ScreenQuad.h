#include "Point2D.h"
#include "Simple2D.h"
#include <glad\glad.h>
#include "Vector3.h"

class ScreenQuad
{
public:
	ScreenQuad(GLint texture);
	~ScreenQuad();

	void Draw();

private:
	GLuint VAO;
	GLint tex;
};

