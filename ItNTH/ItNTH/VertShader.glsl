#version 430 core

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 inColor;
layout (location = 2) in vec2 inTexCoord;
layout (location = 3) in vec3 aOffset;

out vec3 color;
out vec2 TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

//uniform vec3 offsets[5];

void main()
{
  //vec3 offset = offsets[gl_InstanceID];
  
  //gl_Position = projection * view * model * vec4(Position, 1.0);
  gl_Position = projection * view * model * vec4(Position + aOffset, 1.0);

  color = inColor;
  TexCoord = inTexCoord;
}