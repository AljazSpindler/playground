#include "Box.h"
#include <cstdlib>
#include "GLFW\glfw3.h"

Box::Box(Point2D* origin, float widthP, float heightP, Color* color, GLint texture, float speed)
{
	width = widthP;
	height = heightP;

	float verticesTemp[32] =
	{   origin->x, origin->y, 0.0f, color->r, color->g, color->b, 0.0f, 0.0f, // 0 -> position(3), color(3), texCoords(2)
		origin->x + width, origin->y, 0.0f, color->r, color->g, color->b, 1.0f, 0.0f, // 1
		origin->x + width, origin->y + height, 0.0f, color->r, color->g, color->b, 1.0f, 1.0f, // 2
		origin->x, origin->y + height, 0.0f, color->r, color->g, color->b, 0.0f, 1.0f // 3
	};

	unsigned int indices[] =
	{
		0, 1, 3,
		1, 2, 3
	};

	// Init render here.

	glGenVertexArrays(1, &VAO);

	GLuint VBO;
	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTemp), verticesTemp, GL_STATIC_DRAW);

	GLuint EBO;
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glActiveTexture(GL_TEXTURE0);
	tex = texture;
	glBindTexture(GL_TEXTURE_2D, texture);

	glBindVertexArray(VAO);

	GLint posLoc = 0;
	GLint colLoc = 1;
	GLint texLoc = 2;

	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 8, (void*)0);

	glEnableVertexAttribArray(colLoc);
	glVertexAttribPointer(colLoc, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 8, (void*)(sizeof(float) * 3));

	glEnableVertexAttribArray(texLoc);
	glVertexAttribPointer(texLoc, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 8, (void*)(sizeof(float) * 6));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

	// Offset calculation for instance rendering.
	Vector3 instanceLocs[5] =
	{
		Vector3(140.0f, 140.0f, 0.0f),
		Vector3(200.0f, 200.0f, 0.0f),
		Vector3(200.0f, 140.0f, 0.0f),
		Vector3(280.0f, 200.0f, 0.0f),
		Vector3(400.0f, 300.0f, 0.0f)
	};

	// Instance rendering using instanced arrays.
	unsigned int instanceVBO;
	glGenBuffers(1, &instanceVBO);
	glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vector3) * 5, &instanceLocs[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glVertexAttribDivisor(3, 1);

	glBindVertexArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// End init render here.

	movSpeed = speed;
	position = origin;
	velocity = new Vector3(0.0f, 0.0f, 0.0f);
}

void Box::Draw()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);

	glBindVertexArray(VAO);
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	//glDrawArraysInstanced(GL_TRIANGLES, 0, 6, 5);
	glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, 5);
	glBindVertexArray(0);
	glActiveTexture(GL_TEXTURE0);
}

void Box::Move(int key, int action, float deltaTime)
{
	float weightedSpeed = movSpeed * deltaTime;
	Point2D* movement = new Point2D(0.0f, 0.0f, 0.0f);
	bounce = rand() % 20;

	if (key == GLFW_KEY_RIGHT && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		movement->x += weightedSpeed;

		if (!Collision(movement))
		{
			position->x += weightedSpeed;
		}
		else
		{
			position->x = 800.0f - width - bounce;
		}
	}
	else if (key == GLFW_KEY_LEFT && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		movement->x -= weightedSpeed;

		if (!Collision(movement))
		{
			position->x -= weightedSpeed;
		}
		else
		{
			position->x = 0.0f + bounce;
		}
	}
	else if (key == GLFW_KEY_UP && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		movement->y += weightedSpeed;

		if (!Collision(movement))
		{
			position->y += weightedSpeed;
		}
		else
		{
			position->y = 600.0f - height - bounce;
		}
	}
	else if (key == GLFW_KEY_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		movement->y -= weightedSpeed;

		if (!Collision(movement))
		{
			position->y -= weightedSpeed;
		}
		else
		{
			position->y = 0.0f + bounce;
		}
	}
}

void Box::Look(double xPos, double yPos)
{
}

void Box::Zoom(double xOffset, double yOffset)
{
}

bool Box::Collision(Point2D* movement)
{
	if (movement->x != 0.0f)
	{
		if ((position->x + movement->x + width) > 800.0f || (position->x + movement->x) < 0.0f)
		{
			return true;
		}
	}

	if (movement->y != 0.0f)
	{
		if ((position->y + movement->y + height) > 600.0f || (position->y + movement->y) < 0.0f)
		{
			return true;
		}
	}

	return false;
}


Box::~Box()
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}
