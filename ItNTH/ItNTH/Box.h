#include "Point2D.h"
#include "Simple2D.h"
#include "Vector3.h"
#include <glad\glad.h>

class Box
{
public:
	Box(Point2D* origin, float widthP, float heightP, Color* color, GLint texture, float speed);
	
	void Draw();

	void Move(int key, int action, float deltaTime);
	void Look(double xPos, double yPos);
	void Zoom(double xOffset, double yOffset);

	Point2D* position;
	float bounce;

	~Box();
private:
	bool Collision(Point2D* movement);

	float vertices[32];
	float movSpeed;

	float width;
	float height;

	Vector3* velocity;

	GLuint VAO;
	GLint tex;
};

