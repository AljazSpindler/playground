#pragma once

struct Vertex2DColor
{
	float x;
	float y;
	float r;
	float g;
	float b;

	Vertex2DColor() { x = 0.0f, y = 0.0f, r = 0.0f, g = 0.0f, b = 0.0f; };

	Vertex2DColor(float xCoord, float yCoord, float R, float G, float B)
	{
		x = xCoord;
		y = yCoord;
		r = R;
		g = G;
		b = B;
	};
};