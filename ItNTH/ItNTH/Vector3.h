#ifndef VECTOR3_H
#define VECTOR3_H

#include <math.h>

class Vector3 
{
public:
	Vector3() { x = 0; y = 0; z = 0; };
	Vector3(float a, float b, float c) { x = a; y = b; z = c; };

	void _addScalar(float scalar) { x += scalar; y += scalar; z += scalar; };
	void _subScalar(float scalar) { x -= scalar; y -= scalar; z -= scalar; };
	void _mulScalar(float scalar) { x *= scalar; y *= scalar; z *= scalar; };
	void _divScalar(float scalar) { x /= scalar; y /= scalar; z /= scalar; };
	void _negate() { x *= -1; y *= -1; z *= -1; };

	void _addVector(Vector3* vec) { x += vec->x; y += vec->y; z += vec->z; };
	Vector3* addVector(Vector3* vec) { Vector3* v = new Vector3(x + vec->x, y + vec->y, z + vec->z); return v; };
	void _subVector(Vector3* vec) { vec->_negate(); x += vec->x; y += vec->y; z += vec->z; };

	float magnitude() { return sqrtf(x * x + y * y + z * z); };

	Vector3 norm() { return Vector3(x / magnitude(), y / magnitude(), z / magnitude()); };

	float dotProduct(Vector3* vec) { return x * vec->x + y * vec->y + z * vec->z; };

	Vector3 crossProduct(Vector3* vec) { return Vector3((y * vec->z - z * vec->y), (z * vec->x - x * vec->z), (x * vec->y - y * vec->x)); };

	float x, y, z;

private:
};

#endif