#ifndef TRANSFORM3D_H
#define TRANSFORM3D_H

#define PI 3.14159265358979323846  /* pi */

#include "Vector4.h"
#include "Mat4x4.h"

class Transform3D
{
public:
	static Mat4x4* GetNonUniformScaleMat(float xScale, float yScale, float zScale)
	{
		Mat4x4* mat = new Mat4x4();
		mat->_identity();

		mat->matrix[0] = xScale;
		mat->matrix[5] = yScale;
		mat->matrix[10] = zScale;

		return mat;
	};

	static Vector4* ScaleNonUniform(float xScale, float yScale, float zScale, Vector4* pos)
	{
		Mat4x4* mat = GetNonUniformScaleMat(xScale, yScale, zScale);
		return mat->mulVector4(pos);
	};

	static Vector4* ScaleUniform(float scale, Vector4* pos)
	{
		Mat4x4* mat = GetNonUniformScaleMat(scale, scale, scale);
		return mat->mulVector4(pos);
	};

	static Mat4x4* GetTranslationMat(float tx, float ty, float tz)
	{
		Mat4x4* mat = new Mat4x4();
		mat->_identity();

		mat->matrix[3] = tx;
		mat->matrix[7] = ty;
		mat->matrix[11] = tz;

		return mat;
	}

	static Vector4* TranslateVec4(float tx, float ty, float tz, Vector4* pos)
	{
		Mat4x4* mat = GetTranslationMat(tx, ty, tz);
		return mat->mulVector4(pos);
	}

	static Mat4x4* GetRotationMat(float angleDegrees, Vector3* rotAxis)
	{
		Mat4x4* mat = new Mat4x4();
		mat->_identity();

		float angleRadians = angleDegrees * (PI / 180.0f);

		mat->matrix[0] = cosf(angleRadians) + ((rotAxis->x * rotAxis->x) * (1 - cosf(angleRadians)));
		mat->matrix[1] = rotAxis->x * rotAxis->y * (1 - cosf(angleRadians)) - rotAxis->z * sinf(angleRadians);
		mat->matrix[2] = rotAxis->x * rotAxis->z * (1 - cosf(angleRadians)) + rotAxis->y * sinf(angleRadians);
		mat->matrix[3] = 0.0f;

		mat->matrix[4] = rotAxis->y * rotAxis->x * (1 - cosf(angleRadians)) + rotAxis->z * sinf(angleRadians);
		mat->matrix[5] = cosf(angleRadians) + ((rotAxis->y * rotAxis->y) * (1 - cosf(angleRadians)));
		mat->matrix[6] = rotAxis->y * rotAxis->z * (1 - cosf(angleRadians)) - rotAxis->x * sinf(angleRadians);
		mat->matrix[7] = 0.0f;

		mat->matrix[8] = rotAxis->z * rotAxis->x * (1 - cosf(angleRadians)) - rotAxis->y * sinf(angleRadians);
		mat->matrix[9] = rotAxis->z * rotAxis->y * (1 - cosf(angleRadians)) + rotAxis->x * sinf(angleRadians);
		mat->matrix[10] = cosf(angleRadians) + ((rotAxis->z * rotAxis->z) * (1 - cosf(angleRadians)));
		mat->matrix[11] = 0.0f;

		mat->matrix[12] = 0.0f;
		mat->matrix[13] = 0.0f;
		mat->matrix[14] = 0.0f;
		mat->matrix[15] = 1.0f;

		return mat;
	}

	static Mat4x4* GetRotationXMat(float angleDegrees)
	{
		Mat4x4* mat = new Mat4x4();
		mat->_identity();

		float angleRadians = angleDegrees * (PI / 180.0f);

		mat->matrix[0] = 1.0f;
		mat->matrix[1] = 0.0f;
		mat->matrix[2] = 0.0f;
		mat->matrix[3] = 0.0f;

		mat->matrix[4] = 0.0f;
		mat->matrix[5] = cosf(angleRadians);
		mat->matrix[6] = -sinf(angleRadians);
		mat->matrix[7] = 0.0f;

		mat->matrix[8] = 0.0f;
		mat->matrix[9] = sinf(angleRadians);
		mat->matrix[10] = cosf(angleRadians);
		mat->matrix[11] = 0.0f;

		mat->matrix[12] = 0.0f;
		mat->matrix[13] = 0.0f;
		mat->matrix[14] = 0.0f;
		mat->matrix[15] = 1.0f;

		return mat;
	}

	static Mat4x4* GetRotationYMat(float angleDegrees)
	{
		Mat4x4* mat = new Mat4x4();
		mat->_identity();

		float angleRadians = angleDegrees * (PI / 180.0f);

		mat->matrix[0] = cosf(angleRadians);
		mat->matrix[1] = 0.0f;
		mat->matrix[2] = sinf(angleRadians);
		mat->matrix[3] = 0.0f;

		mat->matrix[4] = 0.0f;
		mat->matrix[5] = 1.0f;
		mat->matrix[6] = 0.0f;
		mat->matrix[7] = 0.0f;

		mat->matrix[8] = -sinf(angleRadians);
		mat->matrix[9] = 0.0f;
		mat->matrix[10] = cosf(angleRadians);
		mat->matrix[11] = 0.0f;

		mat->matrix[12] = 0.0f;
		mat->matrix[13] = 0.0f;
		mat->matrix[14] = 0.0f;
		mat->matrix[15] = 1.0f;

		return mat;
	}

	static Mat4x4* GetRotationZMat(float angleDegrees)
	{
		Mat4x4* mat = new Mat4x4();
		mat->_identity();

		float angleRadians = angleDegrees * (PI / 180.0f);

		mat->matrix[0] = cosf(angleRadians);
		mat->matrix[1] = -sinf(angleRadians);
		mat->matrix[2] = 0.0f;
		mat->matrix[3] = 0.0f;

		mat->matrix[4] = sinf(angleRadians);
		mat->matrix[5] = cosf(angleRadians);
		mat->matrix[6] = 0.0f;
		mat->matrix[7] = 0.0f;

		mat->matrix[8] = 0.0f;
		mat->matrix[9] = 0.0f;
		mat->matrix[10] = 1.0f;
		mat->matrix[11] = 0.0f;

		mat->matrix[12] = 0.0f;
		mat->matrix[13] = 0.0f;
		mat->matrix[14] = 0.0f;
		mat->matrix[15] = 1.0f;

		return mat;
	}

	static Vector4* RotateVec4(float angleDegrees, Vector3* rotAxis, Vector4* pos)
	{
		Mat4x4* mat = GetRotationMat(angleDegrees, rotAxis);
		return mat->mulVector4(pos);
	}

	static Mat4x4* GetPerspectiveProjMat(float fovInDegrees, float aspectRatio, float front, float back)
	{
		Mat4x4* mat = new Mat4x4();
		mat->_identity();

		float fovRadians = fovInDegrees * (PI / 180.0f);
		float tangent = tanf(fovRadians / 2);
		float height = front * tangent;
		float width = height * aspectRatio;

		float left = -width;
		float right = width;
		float bottom = -height;
		float top = height;

		mat->matrix[0] = (2 * front) / (right - left);
		mat->matrix[1] = 0.0f;
		mat->matrix[2] = (right + left) / (right - left);
		mat->matrix[3] = 0.0f;

		mat->matrix[4] = 0.0f;
		mat->matrix[5] = (2 * front) / (top - bottom);
		mat->matrix[6] = (top + bottom) / (top - bottom);
		mat->matrix[7] = 0.0f;

		mat->matrix[8] = 0.0f;
		mat->matrix[9] = 0.0f;
		mat->matrix[10] = -(back + front) / (back - front);
		mat->matrix[11] = (-2 * back * front) / (back - front);

		mat->matrix[12] = 0.0f;
		mat->matrix[13] = 0.0f;
		mat->matrix[14] = -1.0f;
		mat->matrix[15] = 0.0f;

		return mat;
	}

	static Mat4x4* GetLookAtMat(Vector3* right, Vector3* up, Vector3* direction, Vector3* position)
	{
		Mat4x4* mat = new Mat4x4();
		mat->_identity();

		mat->matrix[0] = right->x;
		mat->matrix[1] = right->y;
		mat->matrix[2] = right->z;
		mat->matrix[3] = 0.0f;

		mat->matrix[4] = up->x;
		mat->matrix[5] = up->y;
		mat->matrix[6] = up->z;
		mat->matrix[7] = 0.0f;

		mat->matrix[8] = direction->x;
		mat->matrix[9] = direction->y;
		mat->matrix[10] = direction->z;
		mat->matrix[11] = 0.0f;

		mat->matrix[12] = 0.0f;
		mat->matrix[13] = 0.0f;
		mat->matrix[14] = 0.0f;
		mat->matrix[15] = 1.0f;

		Mat4x4* mat2 = new Mat4x4();
		mat2->_identity();

		mat2->matrix[0] = 1.0f;
		mat2->matrix[1] = 0.0f;
		mat2->matrix[2] = 0.0f;
		mat2->matrix[3] = -position->x;

		mat2->matrix[4] = 0.0f;
		mat2->matrix[5] = 1.0f;
		mat2->matrix[6] = 0.0f;
		mat2->matrix[7] = -position->y;

		mat2->matrix[8] = 0.0f;
		mat2->matrix[9] = 0.0f;
		mat2->matrix[10] = 1.0f;
		mat2->matrix[11] = -position->z;

		mat2->matrix[12] = 0.0f;
		mat2->matrix[13] = 0.0f;
		mat2->matrix[14] = 0.0f;
		mat2->matrix[15] = 1.0f;

		return mat->mulMatrix(mat2);
	}

	static Mat4x4* GetLookAtMat(Vector3* position, Vector3* target, Vector3* up)
	{
		Vector3* direction = new Vector3(*position);
		direction->_subVector(target);
		direction = &direction->norm();

		Vector3* right = new Vector3(*up);
		right = &right->crossProduct(direction);
		right = &right->norm();
		Vector3* cameraUp = direction;
		cameraUp = &cameraUp->crossProduct(right);

		Mat4x4* mat = new Mat4x4();
		mat->_identity();

		mat->matrix[0] = right->x;
		mat->matrix[1] = right->y;
		mat->matrix[2] = right->z;
		mat->matrix[3] = 0.0f;

		mat->matrix[4] = cameraUp->x;
		mat->matrix[5] = cameraUp->y;
		mat->matrix[6] = cameraUp->z;
		mat->matrix[7] = 0.0f;

		mat->matrix[8] = direction->x;
		mat->matrix[9] = direction->y;
		mat->matrix[10] = direction->z;
		mat->matrix[11] = 0.0f;

		mat->matrix[12] = 0.0f;
		mat->matrix[13] = 0.0f;
		mat->matrix[14] = 0.0f;
		mat->matrix[15] = 1.0f;

		Mat4x4* mat2 = new Mat4x4();
		mat2->_identity();

		mat2->matrix[0] = 1.0f;
		mat2->matrix[1] = 0.0f;
		mat2->matrix[2] = 0.0f;
		mat2->matrix[3] = -position->x;

		mat2->matrix[4] = 0.0f;
		mat2->matrix[5] = 1.0f;
		mat2->matrix[6] = 0.0f;
		mat2->matrix[7] = -position->y;

		mat2->matrix[8] = 0.0f;
		mat2->matrix[9] = 0.0f;
		mat2->matrix[10] = 1.0f;
		mat2->matrix[11] = -position->z;

		mat2->matrix[12] = 0.0f;
		mat2->matrix[13] = 0.0f;
		mat2->matrix[14] = 0.0f;
		mat2->matrix[15] = 1.0f;

		return mat->mulMatrix(mat2);
	}

	static Mat4x4* GetOrthoProjMat(float left, float right, float bottom, float top, float nearP, float farP)
	{
		Mat4x4* mat = new Mat4x4();
		mat->_identity();

		mat->matrix[0] = 2.0f / (right - left);
		mat->matrix[1] = 0.0f;
		mat->matrix[2] = 0.0f;
		mat->matrix[3] = -((right + left) / (right - left));

		mat->matrix[4] = 0.0f;
		mat->matrix[5] = 2.0f / (top - bottom);
		mat->matrix[6] = 0.0f;
		mat->matrix[7] = -((top + bottom) / (top - bottom));

		mat->matrix[8] = 0.0f;
		mat->matrix[9] = 0.0f;
		mat->matrix[10] = (-2.0f) / (farP - nearP);
		mat->matrix[11] = -((farP + nearP) / (farP - nearP));

		mat->matrix[12] = 0.0f;
		mat->matrix[13] = 0.0f;
		mat->matrix[14] = 0.0f;
		mat->matrix[15] = 1.0f;

		return mat;
	}

private:

	Transform3D() {};

};



#endif
