#include "Point2D.h"
#include "Simple2D.h"
#include <glad\glad.h>
#include "Vector3.h"

class Plane
{
public:
	Plane(Vector3* cent, Color* color, GLint texture, GLint texture2, Vector3* norm, float h, float w);
	~Plane();

	void Draw();
	Vector3* center;

private:
	Vector3* normal;
	float width;
	float height;

	GLuint VAO;
	GLint tex;
	GLint tex2;
};

