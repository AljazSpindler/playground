#include "Point2D.h"
#include "Simple2D.h"
#include "Vector3.h"
#include <glad\glad.h>

class Ball
{
public:
	Ball(Point2D* origin, float rad, Color* color, GLint texture, float speed);
	void Draw();

	void Move(int key, int action, float deltaTime);
	void Look(double xPos, double yPos);
	void Zoom(double xOffset, double yOffset);

	Point2D* position;
	float bounce;
	float radius;

	~Ball();
private:
	bool Collision(Point2D* movement);

	float vertices[32];
	float movSpeed;

	Vector3* velocity;

	GLuint VAO;
	GLint tex;
};

