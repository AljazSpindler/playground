#ifndef VECTOR4_H
#define VECTOR4_H

#include <math.h>

class Vector4
{
public:
	Vector4() { x = 0; y = 0; z = 0; w = 0; };
	Vector4(float a, float b, float c, float d) { x = a; y = b; z = c; w = d; };

	void _addScalar(float scalar) { x += scalar; y += scalar; z += scalar; w += scalar; };
	void _subScalar(float scalar) { x -= scalar; y -= scalar; z -= scalar; w -= scalar; };
	void _mulScalar(float scalar) { x *= scalar; y *= scalar; z *= scalar; w *= scalar; };
	void _divScalar(float scalar) { x /= scalar; y /= scalar; z /= scalar; w /= scalar; };
	void _negate() { x *= -1; y *= -1; z *= -1; w *= -1; };

	void _addVector(Vector4* vec) { x += vec->x; y += vec->y; z += vec->z; w += vec->w; };
	void _subVector(Vector4* vec) { vec->_negate(); x += vec->x; y += vec->y; z += vec->z; w += vec->w; };

	float magnitude() { return sqrtf(x * x + y * y + z * z + w * w); };

	Vector4 norm() { return Vector4(x / magnitude(), y / magnitude(), z / magnitude(), w / magnitude()); };

	float dotProduct(Vector4* vec) { return x * vec->x + y * vec->y + z * vec->z + w * vec->w; };

	Vector4 crossProduct(Vector4* vec) { return Vector4((y * vec->z - z * vec->y), (z * vec->x - x * vec->z), (x * vec->y - y * vec->x), 1); }; // Is not actually defined.

	float x, y, z, w;

private:
};

#endif
