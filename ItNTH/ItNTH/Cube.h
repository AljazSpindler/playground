#include "Point2D.h"
#include "Simple2D.h"
#include <glad\glad.h>
#include "Vector3.h"

class Cube
{
public:
	Cube(Color* color, GLint texture, GLint texture2, Vector3* positionIn);
	~Cube();

	void Draw(bool points);
	Vector3* position;
private:
	GLuint VAO;
	GLint tex;
	GLint tex2;
};

