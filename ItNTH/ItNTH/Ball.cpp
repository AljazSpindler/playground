#include "Ball.h"
#include <cstdlib>
#include "GLFW\glfw3.h"

# define PI           3.14159265358979323846  /* pi */

Ball::Ball(Point2D* origin, float rad, Color* color, GLint texture, float speed)
{
	const int segNum = 16;
	radius = rad;

	float initialAngle = (2 * PI / segNum);
	float lengthAC = rad * cos(initialAngle / 2);
	float lengthBCCD = rad *  sin(initialAngle / 2);

	float cCoordY = origin->y + lengthAC;

	float cCoordX = origin->x + lengthBCCD;

	// A point on the circle.
	float xb = cCoordX - lengthBCCD;
	float yb = origin->y + lengthAC;

	float xStart = xb;
	float yStart = yb;

	float s = sin(initialAngle);
	float c = cos(initialAngle);

	float vertices[17 * segNum];
	int j = 0;
	for (int i = 0; i < segNum; i++)
	{
		vertices[j * 8 + 0] = origin->x; vertices[j * 8 + 1] = origin->y; vertices[j * 8 + 2] = 0.0f;
		vertices[j * 8 + 3] = color->r; vertices[j * 8 + 4] = color->g; vertices[j * 8 + 5] = color->b;
		vertices[j * 8 + 6] = j * 0.125f; vertices[j * 8 + 7] = j * 0.125f;
		j++;

		vertices[j * 8 + 0] = xb; vertices[j * 8 + 1] = yb; vertices[j * 8 + 2] = 0.0f;
		vertices[j * 8 + 3] = color->r; vertices[j * 8 + 4] = color->g; vertices[j * 8 + 5] = color->b;
		vertices[j * 8 + 6] = j * 0.1f; vertices[j * 8 + 7] = j * 0.1f;
		j++;

		// Translate point back to origin.
		xb -= origin->x;
		yb -= origin->y;

		// Rotate point. xNew = x*cos(a) - y*sin(a); yNew = y*cos(a) + x*sin(a)
		float xnew = xb * c - yb * s;
		float ynew = xb* s + yb * c;

		// Translate point back.
		xb = xnew + origin->x;
		yb = ynew + origin->y;
	}

	vertices[j * 8 + 0] = xStart; vertices[j * 8 + 1] = yStart; vertices[j * 8 + 2] = 0.0f;
	vertices[j * 8 + 3] = color->r; vertices[j * 8 + 4] = color->g; vertices[j * 8 + 5] = color->b;
	vertices[j * 8 + 6] = 0.0f; vertices[j * 8 + 7] = 0.0f;


	glGenVertexArrays(1, &VAO);

	GLuint VBO;
	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glActiveTexture(GL_TEXTURE0);
	tex = texture;
	glBindTexture(GL_TEXTURE_2D, texture);

	glBindVertexArray(VAO);

	GLint posLoc = 0;
	GLint colLoc = 1;
	GLint texLoc = 2;

	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 8, (void*)0);

	glEnableVertexAttribArray(colLoc);
	glVertexAttribPointer(colLoc, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 8, (void*)(sizeof(float) * 3));

	glEnableVertexAttribArray(texLoc);
	glVertexAttribPointer(texLoc, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 8, (void*)(sizeof(float) * 6));

	glBindVertexArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	movSpeed = speed;
	position = origin;
	velocity = new Vector3(0.0f, 0.0f, 0.0f);
}

void Ball::Draw()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);

	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 34);
	glBindVertexArray(0);
	glActiveTexture(GL_TEXTURE0);
}

void Ball::Move(int key, int action, float deltaTime)
{
	float weightedSpeed = movSpeed * deltaTime;
	Point2D* movement = new Point2D(0.0f, 0.0f, 0.0f);
	bounce = rand() % 20;

	if (key == GLFW_KEY_D && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		movement->x += weightedSpeed;

		if (!Collision(movement))
		{
			position->x += weightedSpeed;
		}
		else
		{
			position->x = 800.0f - 2*radius - bounce;
		}
	}
	else if (key == GLFW_KEY_A && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		movement->x -= weightedSpeed;

		if (!Collision(movement))
		{
			position->x -= weightedSpeed;
		}
		else
		{
			position->x = 0.0f  + radius + bounce;
		}
	}
	else if (key == GLFW_KEY_W && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		movement->y += weightedSpeed;

		if (!Collision(movement))
		{
			position->y += weightedSpeed;
		}
		else
		{
			position->y = 600.0f - 2*radius - bounce;
		}
	}
	else if (key == GLFW_KEY_S && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		movement->y -= weightedSpeed;

		if (!Collision(movement))
		{
			position->y -= weightedSpeed;
		}
		else
		{
			position->y = 0.0f + radius + bounce;
		}
	}
}

void Ball::Look(double xPos, double yPos)
{
}

void Ball::Zoom(double xOffset, double yOffset)
{
}

bool Ball::Collision(Point2D* movement)
{
	if (movement->x != 0.0f)
	{
		if ((position->x + movement->x + radius) > 800.0f || (position->x + movement->x) < 0.0f)
		{
			return true;
		}
	}

	if (movement->y != 0.0f)
	{
		if ((position->y + movement->y + radius) > 600.0f || (position->y + movement->y) < 0.0f)
		{
			return true;
		}
	}

	return false;
}


Ball::~Ball()
{
}
