#version 430 core

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 inColor;
layout (location = 2) in vec2 inTexCoord;

//out VS_OUT
//{
//  vec3 color;
//  vec2 TexCoord;
//} vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
  gl_Position = projection * view * model * vec4(Position, 1.0);
  gl_PointSize = gl_Position.z;
  //vs_out.color = inColor;
  //vs_out.TexCoord = inTexCoord;
}