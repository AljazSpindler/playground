#ifndef MAT3X3_H
#define MAT3X3_H

#include <math.h>
#include "Vector3.h"

class Mat3x3
{
public:
	Mat3x3()
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] = 0;
		}
	};

	void addScalar(float scalar)
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] += scalar;
		}
	};

	void subScalar(float scalar)
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] -= scalar;
		}
	};

	void mulScalar(float scalar)
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] *= scalar;
		}
	};

	void addMatrix(Mat3x3* mat)
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] += mat->matrix[i];
		}
	};

	void subMatrix(Mat3x3* mat)
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] -= mat->matrix[i];
		}
	};

	Mat3x3* mulMatrix(Mat3x3* mat)
	{
		Mat3x3* ret = new Mat3x3();
		ret->matrix[0] = matrix[0] * mat->matrix[0] + matrix[1] * mat->matrix[3] + matrix[2] * mat->matrix[6];
		ret->matrix[1] = matrix[0] * mat->matrix[1] + matrix[1] * mat->matrix[4] + matrix[2] * mat->matrix[7];
		ret->matrix[2] = matrix[0] * mat->matrix[2] + matrix[1] * mat->matrix[5] + matrix[2] * mat->matrix[8];

		ret->matrix[3] = matrix[3] * mat->matrix[0] + matrix[4] * mat->matrix[3] + matrix[5] * mat->matrix[6];
		ret->matrix[4] = matrix[3] * mat->matrix[1] + matrix[4] * mat->matrix[4] + matrix[5] * mat->matrix[7];
		ret->matrix[5] = matrix[3] * mat->matrix[2] + matrix[4] * mat->matrix[5] + matrix[5] * mat->matrix[8];
		
		ret->matrix[6] = matrix[6] * mat->matrix[0] + matrix[7] * mat->matrix[3] + matrix[8] * mat->matrix[6];
		ret->matrix[7] = matrix[6] * mat->matrix[1] + matrix[7] * mat->matrix[4] + matrix[8] * mat->matrix[7];
		ret->matrix[8] = matrix[6] * mat->matrix[2] + matrix[7] * mat->matrix[5] + matrix[8] * mat->matrix[8];

		return ret;
	};

	Vector3* mulVector3(Vector3* vec)
	{
		Vector3* ret = new Vector3();

		ret->x = matrix[0] * vec->x + matrix[1] * vec->y + matrix[2] * vec->z;
		ret->y = matrix[3] * vec->x + matrix[4] * vec->y + matrix[5] * vec->z;;
		ret->z = matrix[6] * vec->x + matrix[7] * vec->y + matrix[8] * vec->z;;
		
		return ret;
	}

	void _identity()
	{
		matrix[0] = 1; matrix[1] = 0; matrix[2] = 0;
		matrix[3] = 0; matrix[4] = 1; matrix[5] = 0;
		matrix[6] = 0; matrix[7] = 0; matrix[8] = 1;
	};

	static const int rows = 3;
	static const int columns = 3;
	float matrix[9];

private:

};

#endif
