#include "FPCamera.h"
#include <GLFW\glfw3.h>

FPCamera::FPCamera(Vector3 camPos, Vector3 camFront, Vector3 camUp, float camSpeed, float camSensitivity)
{
	pos = camPos;
	front = camFront;
	up = camUp;
	speed = camSpeed;
	sensitivity = camSensitivity;

	yaw = 0.0f;
	pitch = 0.0f;
	firstMove = true;
	lastX = 400;
	lastY = 400;

	fov = 45.0f;
}

void FPCamera::Move(int key, int action, float deltaTime)
{
	float weightedSpeed = speed * deltaTime;
	if (key == GLFW_KEY_RIGHT && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		Vector3 cross = Vector3(front);
		cross = cross.crossProduct(&up);
		cross = cross.norm();
		cross._mulScalar(weightedSpeed);
		pos._addVector(&cross);
	}
	else if (key == GLFW_KEY_LEFT && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		Vector3 cross = Vector3(front);
		cross = cross.crossProduct(&up);
		cross = cross.norm();
		cross._mulScalar(weightedSpeed);
		pos._subVector(&cross);
	}
	else if (key == GLFW_KEY_UP && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		Vector3 temp = Vector3(front);
		temp._mulScalar(weightedSpeed);
		pos._addVector(&temp);
	}
	else if (key == GLFW_KEY_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		Vector3 temp = Vector3(front);
		temp._mulScalar(weightedSpeed);
		pos._subVector(&temp);
	}
}

void FPCamera::Look(double xPos, double yPos)
{
	if (firstMove)
	{
		lastX = xPos;
		lastY = yPos;
		firstMove = false;
	}

	float xOffset = xPos - lastX;
	float yOffset = lastY - yPos;
	lastX = xPos;
	lastY = yPos;

	xOffset *= sensitivity;
	yOffset *= sensitivity;

	yaw += xOffset;
	pitch += yOffset;

	if (pitch > 89.0f)
	{
		pitch = 89.0f;
	}
	if (pitch < -89.0f)
	{
		pitch = -89.0f;
	}

	Vector3 frontTemp = Vector3();
	frontTemp.x = cosf(pitch * (PI / 180.0f)) * cosf(yaw * (PI / 180.0f));
	frontTemp.y = sin(pitch * (PI / 180.0f));
	frontTemp.z = cosf(pitch * (PI / 180.0f)) * sinf(yaw * (PI / 180.0f));
	front = frontTemp.norm();
}

void FPCamera::Zoom(double xOffset, double yOffset)
{
	if (fov >= 1.0f && fov <= 45.0f)
	{
		fov -= yOffset;
	}
	if (fov <= 1.0f)
	{
		fov = 1.0f;
	}
	if (fov >= 45.0f)
	{
		fov = 45.0f;
	}
}

Mat4x4* FPCamera::GetLookAtMat()
{
	Vector3* t = pos.addVector(&front);
	Mat4x4* view = Transform3D::GetLookAtMat(&pos, t, &up);
	return view;
}
