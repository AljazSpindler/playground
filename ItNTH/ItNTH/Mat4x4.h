#ifndef MAT4X4_H
#define MAT4X4_H

#include <math.h>
#include "Vector4.h"

class Mat4x4
{
public:
	Mat4x4()
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] = 0;
		}
	};

	void addScalar(float scalar)
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] += scalar;
		}
	};

	void subScalar(float scalar)
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] -= scalar;
		}
	};

	void mulScalar(float scalar)
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] *= scalar;
		}
	};

	void addMatrix(Mat4x4* mat)
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] += mat->matrix[i];
		}
	};

	void subMatrix(Mat4x4* mat)
	{
		for (int i = 0; i < (rows * columns); i++)
		{
			matrix[i] -= mat->matrix[i];
		}
	};

	Mat4x4* mulMatrix(Mat4x4* mat)
	{
		Mat4x4* ret = new Mat4x4();
		ret->matrix[0] = matrix[0] * mat->matrix[0] + matrix[1] * mat->matrix[4] + matrix[2] * mat->matrix[8] + matrix[3] * mat->matrix[12];
		ret->matrix[1] = matrix[0] * mat->matrix[1] + matrix[1] * mat->matrix[5] + matrix[2] * mat->matrix[9] + matrix[3] * mat->matrix[13];
		ret->matrix[2] = matrix[0] * mat->matrix[2] + matrix[1] * mat->matrix[6] + matrix[2] * mat->matrix[10] + matrix[3] * mat->matrix[14];
		ret->matrix[3] = matrix[0] * mat->matrix[3] + matrix[1] * mat->matrix[7] + matrix[2] * mat->matrix[11] + matrix[3] * mat->matrix[15];

		ret->matrix[4] = matrix[4] * mat->matrix[0] + matrix[5] * mat->matrix[4] + matrix[6] * mat->matrix[8] + matrix[7] * mat->matrix[12];
		ret->matrix[5] = matrix[4] * mat->matrix[1] + matrix[5] * mat->matrix[5] + matrix[6] * mat->matrix[9] + matrix[7] * mat->matrix[13];
		ret->matrix[6] = matrix[4] * mat->matrix[2] + matrix[5] * mat->matrix[6] + matrix[6] * mat->matrix[10] + matrix[7] * mat->matrix[14];
		ret->matrix[7] = matrix[4] * mat->matrix[3] + matrix[5] * mat->matrix[7] + matrix[6] * mat->matrix[11] + matrix[7] * mat->matrix[15];

		ret->matrix[8] = matrix[8] * mat->matrix[0] + matrix[9] * mat->matrix[4] + matrix[10] * mat->matrix[8] + matrix[11] * mat->matrix[12];
		ret->matrix[9] = matrix[8] * mat->matrix[1] + matrix[9] * mat->matrix[5] + matrix[10] * mat->matrix[9] + matrix[11] * mat->matrix[13];
		ret->matrix[10] = matrix[8] * mat->matrix[2] + matrix[9] * mat->matrix[6] + matrix[10] * mat->matrix[10] + matrix[11] * mat->matrix[14];
		ret->matrix[11] = matrix[8] * mat->matrix[3] + matrix[9] * mat->matrix[7] + matrix[10] * mat->matrix[11] + matrix[11] * mat->matrix[15];

		ret->matrix[12] = matrix[12] * mat->matrix[0] + matrix[13] * mat->matrix[4] + matrix[14] * mat->matrix[8] + matrix[15] * mat->matrix[12];
		ret->matrix[13] = matrix[12] * mat->matrix[1] + matrix[13] * mat->matrix[5] + matrix[14] * mat->matrix[9] + matrix[15] * mat->matrix[13];
		ret->matrix[14] = matrix[12] * mat->matrix[2] + matrix[13] * mat->matrix[6] + matrix[14] * mat->matrix[10] + matrix[15] * mat->matrix[14];
		ret->matrix[15] = matrix[12] * mat->matrix[3] + matrix[13] * mat->matrix[7] + matrix[14] * mat->matrix[11] + matrix[15] * mat->matrix[15];

		return ret;
	};

	Vector4* mulVector4(Vector4* vec)
	{
		Vector4* ret = new Vector4();

		ret->x = matrix[0] * vec->x + matrix[1] * vec->y + matrix[2] * vec->z + matrix[3] * vec->w;
		ret->y = matrix[4] * vec->x + matrix[5] * vec->y + matrix[6] * vec->z + matrix[7] * vec->w;
		ret->z = matrix[8] * vec->x + matrix[9] * vec->y + matrix[10] * vec->z + matrix[11] * vec->w;
		ret->w = matrix[12] * vec->x + matrix[13] * vec->y + matrix[14] * vec->z + matrix[15] * vec->w;

		return ret;
	}

	void _identity()
	{
		matrix[0] = 1; matrix[1] = 0; matrix[2] = 0; matrix[3] = 0;
		matrix[4] = 0; matrix[5] = 1; matrix[6] = 0; matrix[7] = 0;
		matrix[8] = 0; matrix[9] = 0; matrix[10] = 1; matrix[11] = 0;
		matrix[12] = 0; matrix[13] = 0; matrix[14] = 0; matrix[15] = 1;
	};

	float* getData() { return matrix;  };

	static const int rows = 4;
	static const int columns = 4;
	float matrix[16];

private:

};

#endif

