#include "Simple2D.h"
#include <cstdlib>

# define PI           3.14159265358979323846  /* pi */

Simple2D::Simple2D()
{
}


Simple2D::~Simple2D()
{
}

void Simple2D::DrawRect(Point2D* origin, float width, float height, GLuint* VAO, GLuint* VBO, GLuint* EBO, Color* color, GLint texture, GLint texture2)
{
	// Draw two triangles ofc without the index buffer.

	/*float vertices[32] =
	{   origin->x, origin->y, 0.0f, color->r, color->g, color->b, 0.0f, 0.0f, // 0 -> position(3), color(3), texCoords(2)
		origin->x + width, origin->y, 0.0f, color->r, color->g, color->b, 1.0f, 0.0f, // 1
		origin->x + width, origin->y + height, 0.0f, color->r, color->g, color->b, 1.0f, 1.0f, // 2
		origin->x, origin->y + height, 0.0f, color->r, color->g, color->b, 0.0f, 1.0f // 3
	};

	unsigned int indices[] =
	{
		0, 1, 3,
		1, 2, 3
	};

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture2);

	glBindVertexArray(*VAO);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *EBO);*/
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void Simple2D::DrawCircleFilled(Point2D* center, float radius, int segments, GLuint* VBO, Color* color)
{
	float initialAngle = (2 * PI / float(segments));
	float lengthAC = radius * cos(initialAngle / 2);
	float lengthBCCD = radius *  sin(initialAngle / 2);

	float cCoordY = center->y + lengthAC;

	float cCoordX = center->x + lengthBCCD;

	// A point on the circle.
	float xb = cCoordX - lengthBCCD;
	float yb = center->y + lengthAC;

	float xStart = xb;
	float yStart = yb;

	float s = sin(initialAngle);
	float c = cos(initialAngle);

	glBegin(GL_TRIANGLE_STRIP);

	for (int i = 0; i < segments; i++)
	{
		glVertex2f(xb, yb);
		glVertex2f(center->x, center->y);

		// Translate point back to origin.
		xb -= center->x;
		yb -= center->y;

		// Rotate point. xNew = x*cos(a) - y*sin(a); yNew = y*cos(a) + x*sin(a)
		float xnew = xb * c - yb * s;
		float ynew = xb* s + yb * c;

		// Translate point back.
		xb = xnew + center->x;
		yb = ynew + center->y;
	}

	glVertex2f(xStart, yStart);

	glEnd();
}

void Simple2D::DrawCircle(Point2D* center, float radius, int segments, GLuint* VBO, Color* color)
{
	float initialAngle = (2 * PI / float(segments));
	float lengthAC = radius * cos(initialAngle / 2);
	float lengthBCCD = radius *  sin(initialAngle / 2);

	float cCoordY = center->y + lengthAC;

	float cCoordX = center->x + lengthBCCD;

	// A point on the circle.
	float xb = cCoordX - lengthBCCD;
	float yb = center->y + lengthAC;

	float xStart = xb;
	float yStart = yb;

	float s = sin(initialAngle);
	float c = cos(initialAngle);

	glBegin(GL_LINE_LOOP);

	for (int i = 0; i < segments; i++)
	{
		glVertex2f(xb, yb);

		// Translate point back to origin.
		xb -= center->x;
		yb -= center->y;

		// Rotate point. xNew = x*cos(a) - y*sin(a); yNew = y*cos(a) + x*sin(a)
		float xnew = xb * c - yb * s;
		float ynew = xb* s + yb * c;

		// Translate point back.
		xb = xnew + center->x;
		yb = ynew + center->y;
	}

	glEnd();
}

void Simple2D::DrawTriangle(Point2D* a, Point2D* b, Point2D* c, GLuint* VAO, GLuint* VBO, Color* color)
{
	float vertices[18] = 
	{ a->x, a->y, a->z, color->r, color->g, color->b,
	  b->x, b->y, b->z, color->r, color->g, color->b,
	  c->x, c->y, c->z, color->r, color->g, color->b
	};

	glBindBuffer(GL_ARRAY_BUFFER, *VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBindVertexArray(*VAO);

	glDrawArrays(GL_TRIANGLES, 0, 3);
}
