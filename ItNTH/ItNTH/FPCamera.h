#include "Vector3.h"
#include "Tranform3D.h"

class FPCamera
{
public:
	FPCamera(Vector3 camPos, Vector3 camFront, Vector3 camUp, float camSpeed, float camSensitivity);	
	~FPCamera() {};

	void Move(int key, int action, float deltaTime);
	void Look(double xPos, double yPos);
	void Zoom(double xOffset, double yOffset);
	Mat4x4* GetLookAtMat();

	float GetFov() { return fov; };
	Vector3* GetPos() { return &pos; };
	Vector3* GetFront() { return &front; };

private:
	// Camera vectors.
	Vector3 pos;
	Vector3 front;
	Vector3 up;

	// Camera parameters.
	float speed;
	float yaw;
	float pitch;
	float sensitivity;
	float lastX;
	float lastY;
	bool firstMove;
	float fov;
};

