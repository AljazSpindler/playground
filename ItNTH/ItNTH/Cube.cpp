#include "Cube.h"

Cube::Cube(Color* color, GLint texture, GLint texture2, Vector3* positionIn)
{
	position = positionIn;

	float vertices[396] = {
		-0.5f, -0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 0.0f, 0.0f,  0.0f, -1.0f,
		0.5f, 0.5f, -0.5f, color->r, color->g, color->b,  1.0f, 1.0f, 0.0f,  0.0f, -1.0f,
		0.5f, -0.5f, -0.5f, color->r, color->g, color->b,  1.0f, 0.0f, 0.0f,  0.0f, -1.0f,
		0.5f,  0.5f, -0.5f, color->r, color->g, color->b,  1.0f, 1.0f, 0.0f,  0.0f, -1.0f,
		-0.5f, -0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 0.0f, 0.0f,  0.0f, -1.0f,
		-0.5f, 0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,

		-0.5f, -0.5f,  0.5f, color->r, color->g, color->b,  0.0f, 0.0f, 0.0f,  0.0f, 1.0f,
		0.5f, -0.5f,  0.5f, color->r, color->g, color->b,  1.0f, 0.0f, 0.0f,  0.0f, 1.0f,
		0.5f,  0.5f,  0.5f, color->r, color->g, color->b,  1.0f, 1.0f, 0.0f,  0.0f, 1.0f,
		0.5f,  0.5f,  0.5f, color->r, color->g, color->b,  1.0f, 1.0f, 0.0f,  0.0f, 1.0f,
		-0.5f,  0.5f,  0.5f, color->r, color->g, color->b,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f, color->r, color->g, color->b,  0.0f, 0.0f, 0.0f,  0.0f, 1.0f,

		-0.5f,  0.5f,  0.5f, color->r, color->g, color->b,  1.0f, 0.0f, -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f, color->r, color->g, color->b,  1.0f, 1.0f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 1.0f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 1.0f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f,  0.5f, color->r, color->g, color->b,  0.0f, 0.0f, -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f, color->r, color->g, color->b,  1.0f, 0.0f, -1.0f,  0.0f,  0.0f,

		0.5f, 0.5f,  0.5f, color->r, color->g, color->b,  1.0f, 0.0f, 1.0f,  0.0f,  0.0f,
		0.5f, -0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f,
		0.5f, 0.5f, -0.5f, color->r, color->g, color->b,  1.0f, 1.0f, 1.0f,  0.0f,  0.0f,
		0.5f, -0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f,
		0.5f,  0.5f,  0.5f, color->r, color->g, color->b,  1.0f, 0.0f, 1.0f,  0.0f,  0.0f,
		0.5f, -0.5f,  0.5f, color->r, color->g, color->b, 0.0f, 0.0f, 1.0f,  0.0f,  0.0f,

		-0.5f, -0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 1.0f, 0.0f, -1.0f,  0.0f,
		0.5f, -0.5f, -0.5f, color->r, color->g, color->b,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,
		0.5f, -0.5f,  0.5f, color->r, color->g, color->b,  1.0f, 0.0f, 0.0f, -1.0f,  0.0f,
		0.5f, -0.5f,  0.5f, color->r, color->g, color->b,  1.0f, 0.0f, 0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f, color->r, color->g, color->b,  0.0f, 0.0f, 0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 1.0f, 0.0f, -1.0f,  0.0f,

		-0.5f,  0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 1.0f, 0.0f,  1.0f,  0.0f,
		0.5f,  0.5f, 0.5f, color->r, color->g, color->b,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,
		0.5f,  0.5f, -0.5f, color->r, color->g, color->b,  1.0f, 1.0f, 0.0f,  1.0f,  0.0f,
		0.5f,  0.5f,  0.5f, color->r, color->g, color->b,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f, color->r, color->g, color->b,  0.0f, 1.0f, 0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f, 0.5f, color->r, color->g, color->b,  0.0f, 0.0f, 0.0f,  1.0f,  0.0f

	};

	glGenVertexArrays(1, &VAO);

	GLuint VBO;
	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	tex = texture;
	tex2 = texture2;

	if (tex > -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
	}

	if (tex2 > -1)
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);
	}

	glBindVertexArray(VAO);

	GLint posLoc = 0;
	//GLint colLoc = 1;
	GLint texLoc = 2;
	GLint normalLoc = 1;

	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 11, (void*)0);
	glEnableVertexAttribArray(normalLoc);
	glVertexAttribPointer(normalLoc, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 11, (void*)(sizeof(float) * 8));

	/*glEnableVertexAttribArray(colLoc);
	glVertexAttribPointer(colLoc, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 11, (void*)(sizeof(float) * 3));
	*/
	glEnableVertexAttribArray(texLoc);
	glVertexAttribPointer(texLoc, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 11, (void*)(sizeof(float) * 6));

	glBindVertexArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


Cube::~Cube()
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

void Cube::Draw(bool points)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, tex2);

	glBindVertexArray(VAO);
	if (!points)
	{
		glDrawArrays(GL_TRIANGLES, 0, 36);
	}
	else
	{
		glDrawArrays(GL_POINTS, 0, 36);
	}
	glBindVertexArray(0);
	glActiveTexture(GL_TEXTURE0);
}
