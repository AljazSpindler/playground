#include "Point2D.h"
#include "Simple2D.h"
#include <glad\glad.h>
#include "Vector3.h"

class SkyBox
{
public:
	SkyBox(GLint texture);
	~SkyBox();

	void Draw();
private:
	GLuint VAO;
	GLint tex;
};

